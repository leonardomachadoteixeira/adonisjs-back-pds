'use strict'

const User = use('App/Models/User')
const Hash = use('Hash')
const Mail = use('Mail')

class UserController {

    async index({ response }) {
        let users = await User.all()

        return response.json(users)
    }

    async show({ params, response }) {
        const user = await User.find(params.id)

        return response.json(user)
    }

    async store({ request, response }) {
        const userInfo = request.only(['username', 'email', 'password'])

        if (!userInfo.username || !userInfo.email || !userInfo.password) {
            return response.status(400).json({ data: 'Empty field' })
        }

        const user = new User()
        user.username = userInfo.username
        user.email = userInfo.email
        user.password = userInfo.password

        await user.save()

        return response.status(201).json(user)
    }

    async update({ params, request, response }) {
        const userInfo = request.only(['email'])

        const user = await User.find(params.id)
        if (!user) {
            return response.status(404).json({ data: 'Resource not found' })
        }
        //user.username = userInfo.username
        user.email = userInfo.email
        //user.password = userInfo.password

        await user.save()

        return response.status(200).json(user)
    }

    async updatePassword({ params, request, response }) {
        const userInfo = request.only(['password'])

        const user = await User.find(params.id)
        if (!user) {
            return response.status(404).json({ data: 'Resource not found' })
        }
        //user.username = userInfo.username
        user.password = await Hash.make(userInfo.password)
        //user.password = userInfo.password

        await user.save()

        return response.status(200).json(user)
    }

    async delete({ params, response }) {
        const user = await User.find(params.id)
        if (!user) {
            return response.status(404).json({ data: 'Resource not found' })
        }
        await user.delete()

        return response.status(204).json(null)
    }

    async login({ request, response }) {

        const userInfo = request.only(['username', 'password'])
        const user = await User.findBy('username', userInfo.username)

        if (!user) {
            return response.status(404).json('Usuário não encontrado!')
        }

        const isSame = await Hash.verify(userInfo.password, user.password)

        if (isSame) {
            return response.status(200).json(user)
        }

        return response.status(401).json('Usuário ou senha incorretos!')
    }

    async searchUser({ request, response }) {
        const userInfo = request.only(['username'])
        const user = await User.findBy('username', userInfo.username)

        if (!user) {
            return response.status(100).json('Username não encontrado!')
        }

        return response.status(401).json('Username já utilizado!')
    }

    async userRecover({ request, response }) {

        const userInfo = request.only(['username', 'email'])
        const user = await User.findBy({ 'email': userInfo.email, 'username': userInfo.username })

        if (!user) {
            return response.status(404).json('Ops.. Não encontramos nenhum registro!')
        }

        user.password = await Hash.make('jpm2277')
        await user.save()

        await Mail.send('emails.welcome', {}, (message) => {
            message.from('contatojuntopodemosmais@gmail.com')
            message.to(userInfo.email)
            message.subject('Senha - Juntos Podemos Mais')
        })

    }

}

module.exports = UserController
