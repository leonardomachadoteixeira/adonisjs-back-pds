'use strict'
const Room = use('App/Models/Room')
class RoomController {
    async index({ response }) {
        let rooms = await Room.all()

        return response.json(rooms)
    }

    async show({ params, response }) {
        const room = await Room.find(params.id)

        return response.json(room)
    }

    async store({ request, response }) {
        const roomInfo = request.only(['name', 'description', 'video', 'link', 'iduser'])

        const room = new Room()
        room.name = roomInfo.name
        room.description = roomInfo.description
        room.video = roomInfo.video
        room.link = roomInfo.link
        room.iduser = roomInfo.iduser

        await room.save()

        return response.status(201).json(room)
    }

    async update({ params, request, response }) {
        const roomInfo = request.only(['name', 'description', 'video', 'link'])

        const room = await Room.find(params.id)
        if (!room) {
            return response.status(404).json({ data: 'Resource not found' })
        }
        room.name = roomInfo.name
        room.description = roomInfo.description
        room.video = roomInfo.video
        room.link = roomInfo.link

        await room.save()

        return response.status(200).json(room)
    }

    async delete({ params, response }) {
        const room = await Room.find(params.id)
        if (!room) {
            return response.status(404).json({ data: 'Resource not found' })
        }
        await room.delete()

        return response.status(204).json(null)
    }

    async getroombyiduser({ request, response }) {
        const roomInfo = request.only(['iduser'])
        const rooms = await Room
        .query('SELECT * FROM rooms')
        .where('iduser', '=', roomInfo.iduser)
        .fetch()
        
        if (!rooms) {
            return response.status(404).json({ data: 'Resource not found' })
        }

        return response.status(200).json(rooms)
    }

    async searchRoom({ request, response }) {
        
        const roomInfo = request.only(['name'])
        const room = await Room.findBy('name', roomInfo.name)

        if (!room) {
            return response.status(100).json(null)
        }

        return response.status(401).json('Nome de sala já utilizado!')
    }
}

module.exports = RoomController
