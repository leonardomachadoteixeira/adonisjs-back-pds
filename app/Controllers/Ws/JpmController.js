'use strict'
const util = require('util')
class JpmController {

  constructor({ socket, request }) {
    this.socket = socket
    this.request = request
  }

  onMessage(message) {
    this.socket.broadcastToAll('message', message)
  }

  onAverage(message) {
    this.socket.broadcastToAll('average', message)
  }
}


module.exports = JpmController
