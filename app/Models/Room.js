'use strict'

const Model = use('Model')

class Room extends Model {
    static get table() {
        return 'rooms'
    }

    static get primaryKey() {
        return 'id'
    }
}

module.exports = Room
