'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.0/routing
|
*/

const Route = use('Route')

Route.on('/').render('welcome')
Route.on('favicon.ico', function (req, res) {

})
Route.group(() => {
    Route.post('login', 'UserController.login')
    Route.post('user', 'UserController.store')
    Route.get('users', 'UserController.index')
    Route.get('user/:id', 'UserController.show')
    Route.put('user/:id', 'UserController.update')
    Route.put('userpassword/:id', 'UserController.updatePassword')
    Route.delete('user/:id', 'UserController.delete')
    Route.post('searchUser', 'UserController.searchUser')
    Route.post('user/recover', 'UserController.userRecover')

    Route.post('rooms', 'RoomController.store')
    Route.get('rooms', 'RoomController.index')
    Route.get('rooms/:id', 'RoomController.show')
    Route.put('rooms/:id', 'RoomController.update')
    Route.delete('rooms/:id', 'RoomController.delete')
    Route.post('rooms/user', 'RoomController.getroombyiduser')
    Route.post('rooms/search', 'RoomController.searchRoom')
}).prefix('api/v1')