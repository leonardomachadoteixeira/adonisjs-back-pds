'use strict'

const Schema = use('Schema')

class RoomSchema extends Schema {
  up () {
    this.create('rooms', (table) => {
      table.increments()
      table.string('name').nullable()
      table.string('description').nullable()
      table.string('video').nullable()
      table.string('link').nullable()
      table.string('iduser').nullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('rooms')
  }
}

module.exports = RoomSchema
